---
---

I'm **Neil Hanlon**, a DevOps/Network/Potato Engineer

I love Open Source, Linux, and building cool things with great people.

Feel free to take a peek at some of my [projects], or check out my cat's [Instagram].

For more check out my academic & professional [resume].

Contact me at [@NeilHanlon] or by yelling really really loud.



[projects]: /projects
[resume]: https://shrug.pw/resume.pdf
[@NeilHanlon]: https://twitter.com/NeilHanlon
[instagram]: https://instagram.com/noellathekitty

